package registerBackEnd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.mysql.jdbc.Connection;

	public class users{

		// constantes utilizadas en las ordenes sql
		
		private static final String TABLE = "usuarios";
		private static final String KEY = "id";
		
		// atributos de la clase usuario
		
		private int id;
		private String nombre;
		private String password;
		private String Email;
		// private Date alta;
		
		
		//constructores
		
		public users(String nombre, String password) {
			this.id = 0;
			this.nombre = nombre;
			this.password = password;
		}
		
		public users(int id, String nombre, String password,String email) {
			
			this.id = id;
			this.nombre = nombre;
			this.password = password;
		}
		
		public users(String nombre, String password , String Email) {
			this.id = 0;
			this.nombre = nombre;
			this.password = password;
			this.Email = Email;
		}
		
		
		
		
		//override tostring
		@Override
		public String toString() {
			// usuario se mostrar� como: [id] nombre
			return ""+this.id+" " + this.nombre + "" + this.Email;
		}

		//METODOS ESTATICOS de acceso a BDD
		// se utilizar�n desde la clase: Usuario.getAll(), Usuario.getId(44) �
		

		// List<Usuario> us = Usuario.getAll();
		// STATIC getAll devuelve todos los usuarios
		
		public static List<users> getAll(){
			List<users> usuarioList = new ArrayList<users>();
			String sql = String.format("select id,nombre,password,email from %s", TABLE);
			
			//String sql = "select id,nombre,password from "+TABLE;
			
			try (Connection conn = dbConnect.getConn();
					Statement stmt = conn.createStatement()) {
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					users u = new users(
							rs.getInt(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4));
							usuarioList.add(u);
					}
				
			} catch (Exception e) {
				String s = e.getMessage();
				System.out.println(s);
				}
			
			return usuarioList;
		}

		// Usuario y = Usuario.getId(9);
		// STATIC getId devuelve usuario con id facilitado
		
		

		
		
		public static users getId(int id){
			users u=null;
			String sql = String.format("select id,nombre,password,email from %s where %s=%d", TABLE, KEY, id);
			try (Connection conn = dbConnect.getConn();
					Statement stmt = conn.createStatement()) {
				ResultSet rs = stmt.executeQuery(sql);

				if (rs.next()) {
					u = new users(
							rs.getInt(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4)
							);
				}
			} catch (Exception e) {
				String s = e.getMessage();
				System.out.println(s);
			}
			return u;
		}
		// Ejemplo de uso: Usuario.deleteId(33);
		// ejemplo de uso si no fueste est�tico: 
		// Usuario x = Usuario.getId(33); x.deleteId();

		// STATIC deleteId borra usuario con id facilitado
		
		
		public static int deleteId(int id){
			int resp=-1;
			String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
			
			try (Connection conn = dbConnect.getConn();
					Statement stmt = conn.createStatement()) {
				
				resp = stmt.executeUpdate(sql);
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return resp;
		}
		
		
		// METODOS DE INSTANCIA
		// se utilizand desde objeto: usuario1.save()
		
		// m�todo save(), guarda usuario actual en base de datos
		// si ID > 0 se trata de un usuario existente >>> UPDATE
		// si ID == 0, usuario nuevo >>> INSERT
		
		
		public int save() {
			String sql;
			int resp=-1;
			if (this.id>0) {
				sql = String.format("UPDATE %s set nombre=?, password=?, email=? where %s=%d", TABLE, KEY, this.id);
			}else {
				sql = String.format("INSERT INTO %s (nombre, password, email) VALUES (?,?,?)", TABLE);
			}
			 	
			try (Connection conn = dbConnect.getConn();
					PreparedStatement pstmt = conn.prepareStatement(sql);
					Statement stmt = conn.createStatement()) {

				pstmt.setString(1, this.nombre);
				pstmt.setString(2, this.password);
				pstmt.setString(3, this.Email);

				resp = pstmt.executeUpdate();
				
				if (this.id==0) {
					//usuario nuevo, actualizamos el ID con el reci�n insertado
					
					ResultSet rs = stmt.executeQuery("select last_insert_id()");
						if (rs.next()) {
							this.id=rs.getInt(1);
						}
				}
				
			}catch (Exception e) {
			
				System.out.println(e.getMessage());
			}
		
			return resp;
		}
		
		public int checkUser() {
			int resp= -1;
			String sql;
			
			
			
			
			return 0;
		}
		
		
		
		
		
		
		
		public int getAllUsuario(int x) {
			return x;
		}
		
		
		
		
		
		
		
	
		//GETTER Y SETTERS
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public void setEmail(String Email) {
			this.Email=Email;
		}
	
		public String Email(String Email) {
			return Email;
		}

	
}


	
