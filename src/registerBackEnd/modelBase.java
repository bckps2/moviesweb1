package registerBackEnd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

public abstract class modelBase<T> {

	//metodes que hauran d'implementar les subclasses
	protected abstract String getKeyField();//retorna nom de camp clau, normalment "id"
	protected abstract int getId(); //getter de Id, encara q keyfield no es digui id cal tenir aquest getter
	protected abstract void setId(int id); // setter de Id
	protected abstract String getTable(); //retorna nom taula
	protected abstract String[] getFields(); //string array amb els camps de la taula, sense ID!
	protected abstract T ToModel(ResultSet rs) throws SQLException; //crea un model a partir de retorn de bdd
	protected abstract void fromModel(PreparedStatement pstmt) throws SQLException; // al rev�s, omple els camps de la taula, per ordre!
	

	
	
	// getAll i getAllFilter, diferents variants
	public List<T> getAll(){
		String sql = String.format("select %s, %s FROM %s", getKeyField(), String.join(",",getFields()), getTable());
		return getAll(sql);
		}
	
	
	
	
	public List<T> getAllFilter(String field, String value){
		String sql = String.format("select %s, %s FROM %s WHERE %s=%s", getKeyField(), String.join(",",getFields()), getTable(), field, value);
		return getAll(sql);
	}
	
	
	
	
	public List<T> getAllFilter(String field, int value){
		String sql = String.format("select %s, %s FROM %s WHERE %s=%d", getKeyField(), String.join(",",getFields()), getTable(), field, value);
		return getAll(sql);
	}
	
	public List<T> getAllWhere(String where){
		String sql = String.format("select %s, %s FROM %s WHERE %s", getKeyField(), String.join(",",getFields()), getTable(), where);
		return getAll(sql);
	}
	

	
	// getAll el que fa la consulta real
	public List<T> getAll(String sql){
		
		System.out.println(sql);
		List<T> theList = new ArrayList<T>();
		
		try (Connection conn = dbConnect.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				theList.add(this.ToModel(rs));
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return theList;
	}
	
	
	// getId retorna un sol item
	public T getById(int id){
		String sql = String.format("select %s, %s from %s where %s=%d", getKeyField(), String.join(",",getFields()), getTable(), getKeyField(), id);
		try (Connection conn = dbConnect.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				 return this.ToModel(rs);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		System.out.println(sql);
		return null;
	}
	
	

	// deletes an user by Id
	public int delete(int id){
		int resp=-1;
		String sql = String.format("DELETE FROM %s where %s=%d", getTable(), getKeyField(), id);
		
		try (Connection conn = dbConnect.getConn();
				Statement stmt = conn.createStatement()) {
			resp = stmt.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return resp;
	}
	
	// deletes current user
	public int delete() {
		return this.delete(this.getId());
	}
	
	// m�todo save(), guarda usuario actual en base de datos
	// si ID > 0 se trata de un usuario existente >>> UPDATE
	// si ID == 0, usuario nuevo haremos INSERT
	public int save() {
		String sql;
		int resp=-1;
		String iguals = String.join("=?, ",getFields())+"=? ";
		String[] interrogants = new String[getFields().length];
		for (int i=0; i<getFields().length; i++) {
			interrogants[i]="?";
		}
		if (this.getId()>0) {
			sql = String.format("UPDATE %s set %s where %s=%d", getTable(), iguals, getKeyField(), this.getId());
		}else {
			sql = String.format(
					"INSERT INTO %s (%s) VALUES (%s)", 
					getTable(), 
					String.join(",", getFields()),
					String.join(",", interrogants)
					);
		}
		try (
				Connection conn = dbConnect.getConn();
				PreparedStatement pstmt = conn.prepareStatement(sql);
				Statement stmt = conn.createStatement()
			) {

			fromModel(pstmt);
			resp = pstmt.executeUpdate();
			if (this.getId()==0) {
				//usuario nuevo, actualizamos el ID con el reci�n insertado
				ResultSet rs = stmt.executeQuery("select last_insert_id()");
				if (rs.next()) {
					this.setId(rs.getInt(1));
				}
			}

		} catch (Exception e) {
			// ...
			System.out.println(e.getMessage());
		}
		return resp;
	}

	
}
