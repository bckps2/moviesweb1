package movieBack;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import registerBackEnd.dbConnect;

public class movies {
	
	private static final String TABLE = "listmovies";
	private static final String KEY = "id";


	
	
	private int  id;
	private String titulo;
	private String director;
	private String foto;
	private int year;
	private String sinopsis;
	private int rating;
	private String genero;
	private String Value;
	
	
	
	public movies(int id,String titulo, String director,String foto, int year, String sinopsis, int rating, String genero) {
		this.id=id;
		this.director=director;
		this.foto=foto;
		this.titulo=titulo;
		this.year=year;
		this.sinopsis=sinopsis;
		this.rating=rating;
		this.genero=genero;
	}
	
	public movies(String value) {
		// TODO Auto-generated constructor stub
	}

	/*public String toString (){
        String mensaje="id es : "+id+" Director es : "+director+" a�o :" +year+ " Sinopsis : "+ sinopsis  + " rating :" + rating + " Genero:"+genero;
        return mensaje;
    }*/
	
	
	public static List<movies> getAll(){
		List<movies> movieList = new ArrayList<movies>();
		String sql = String.format("select id,titulo,director,foto,year,sinopsis,rating,genero from %s", TABLE);
		
		//String sql = "select id,nombre,password from "+TABLE;
		
		try (Connection conn = dbConnect.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				movies m = new movies(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getInt(5),
						rs.getString(6),
						rs.getInt(7),
						rs.getString(8)
						);
						movieList.add(m);
				}
			
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
			}
		
		return movieList;
	}


	// Usuario y = Usuario.getId(9);
	// STATIC getId devuelve usuario con id facilitado
	
	
	public static movies getId(int id){
		movies m=null;
		String sql = String.format("select id,titulo,director,foto,year,sinopsis,rating,genero from %s where %s=%d", TABLE, KEY, id);
		
		try (Connection conn = dbConnect.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				m = new movies(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getInt(5),
						rs.getString(6),
						rs.getInt(7),
						rs.getString(8)
						);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		
		System.out.println(m.toString());
		return m;
		
	}

	
	
	
	
	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
	public String getSinopsis() {
		return sinopsis;
	}

	public void setSinopsis(String sinopsis) {
		this.sinopsis = sinopsis;
	}
	
	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
}
